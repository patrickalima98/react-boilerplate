import * as React from "react";

//This will be responsible for handling global system calls and foward it to the class
export default function System ({object, children}) : any {

    //We are going to bind a system/application state to everything
    var [ system, setSystem ] = React.useState({
        fetch:    object.fetch,
        api:      object.api,
        redirect: object.redirect,
        error:    object.error,
        user:     object.state.user
    });

    //Bind to window object
    window.system     = system;
    window.setSystem  = setSystem;

    //Force a null response
    return children;
}