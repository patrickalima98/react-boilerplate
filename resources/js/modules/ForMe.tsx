//Core
import * as React from 'react';

//Standarlized context object
const GroupContext = React.createContext("");

//This will wrap all the ForMe data and transpose it to the inputs
export function Form (props) {
	var [ form, setForm ] = React.useState({});

	return (
		<form>
			<GroupContext.Provider value={""}>
				{props.children}
			</GroupContext.Provider>
		</form>
	);
}

//Group inputs so they may be an object
export function Group ({children, name}) {
	//Get the positioning of the current group
	let context = React.useContext(GroupContext);

	//update the context
	context = context == ""? name:(context + "/" + name);

	//Just pass the children
	return children;
}

//Responsible for setting and saving data
export function Input (props) {
	//get the input final position
	let context = React.useContext(GroupContext);

	console.log(context);

	return (
		<input

		/>
	);
}