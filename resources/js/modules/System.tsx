//Core
import * as React                   from 'react';
import {Redirect, BrowserRouter}    from "react-router-dom";

//Modules
import App                              from  "./App";
import SystemData                       from "./SystemData";

//Components
import ErrorComponent                   from "../components/ErrorComponent";
import LoadingComponent                 from "../components/LoadingComponent";
import WaitingComponent                 from "../components/WaitingComponent";

//Packages
import { ToastContainer, toast }        from 'react-toastify';

//Class responsible for handling everything under the hood for our application to work
export default class System extends React.Component  {

    state = {
        user:           null,
        error:          null,
        loading:        true,
        waiting:        true,
        redirected:     false,
        redirecturl:    "",
        token:          "",
    }

    //-------------------------------------------------
    // Main functions
    //-------------------------------------------------

    //React's render method responsible into turning virtual dom into dom
    public render() {
        //Render redirection
        if (this.state.redirecturl && !this.state.redirected) {
            return (
                <BrowserRouter basename={"/"}>
                    <Redirect to={this.state.redirecturl} />
                </BrowserRouter>
            );
        }

        //Render error component
        if (this.state.error) {
            return <ErrorComponent error={this.state.error} />;
        }

        //Render normal component
        return (
            <BrowserRouter basename={"/"}>
                <SystemData object={this}>
                    <App/>
                    <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} />
                    <LoadingComponent render={this.state.loading} />
                    <WaitingComponent render={this.state.waiting} />
                </SystemData>
            </BrowserRouter>
        );
    }

    //-------------------------------------------------
    // Fetch functions
    //-------------------------------------------------

    //Simple fetch that doesn't require auth
    fetch (target: any, options:any, onSuccess: any = null, onError: any = null) {
        //Is the system waiting?
        if (this.state.waiting) return;

        //Gather special options
        const { wait, toastify } = options;

        //Disable the page while it loads
        if (wait) this.setState({waiting:true});

        //The API request
        return fetch (window.url(target), options).
        then(res => {
            if (!res.ok && res.status == 500) {
                this.setState({error:res.statusText});
            }
            else if (!res.ok && res.status == 401) {
                sessionStorage.removeItem("token");

                //Set state
                this.setState({
                    token:  null,
                    user:   null,
                })
            }

            //Reenable the page after it loads
            if (wait) this.setState({waiting:false});

            return res;
        }).
        then(res => res.json()).
        then(response => {
            //Returned a non fatal error from the server
            if (response.error && toastify) {

                if (!Array.isArray(response.error) && !(typeof response.error === 'object')) {
                    toast.warn(response.error);
                }
                else {

                    for (let index in response.error) {
                        toast.warn(response.error[index][0]);
                    }
                }

                if (onError) onError(response);
            }

            //Everything went right
            else if (response.success && toastify) {
                if (!Array.isArray(response.success) && !(typeof response.success === 'object')) {
                    toast.success(response.success);
                }
                else {
                    for (let index in response.success) {
                        toast.success(response.success[index]);
                    }
                }

                if (onSuccess) onSuccess(response);
            }

            //No feedback given, just consider successfull
            else if (onSuccess) {
                onSuccess(response);
            }
        });
    }

    //Elaborated fetch that handles auth
    api (target, options, onSuccess = null, onError = null) {
        let preheader = {
            headers: {
                "Authorization":  "Bearer " + this.state.token,
                "Content-Type":   "application/json",
                "Accept":         "application/json"
            },
            wait:true,
            toastify:true
        };

        return this.fetch("api/" + target, Object.assign(preheader, options), onSuccess, onError);
    }

    //-------------------------------------------------
    // Redirect functions
    //-------------------------------------------------

    redirect (url = "/") {
        this.setState({
            redirecturl:    url,
            redirected:     false,
        });
    }

    //-------------------------------------------------
    // Error functions
    //-------------------------------------------------

    //Will display an error and prevent the react from crashing
    error (error) {
        this.setState({error});
    }

    componentDidCatch(error, info) {
        this.setState({error});
    }
}
