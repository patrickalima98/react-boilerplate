//Core
import * as React 	            from 'react';
import {Switch}                 from "react-router-dom";

//Libraries
import {Form, Group, Input}     from "./ForMe";

export default function App (props) {

    //We are going to bind a system/application state to everything
    var [ app, setApp ] = React.useState({

    });

    //Bind to window object
    window.system     = app;
    window.setSystem  = setApp;

    //React's render responsible into turning virtual dom into dom
    return (
        <React.Fragment>
            <Form>
                <Group name="user">
                    <Input name="email" />
                    <Input name="name"  />
                </Group>

                <Input name="message" />
            </Form>
        </React.Fragment>
    );
}