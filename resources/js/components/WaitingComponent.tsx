import * as React from 'react';

export default function WaitingComponent ({render}) {
	return (
		<div className={"fullscreen " + (render? "":"d-none")}>
			<h2>Waiting</h2>
		</div>
	);
}