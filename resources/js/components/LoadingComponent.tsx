import * as React from 'react';

export default function LoadingComponent ({render}) {
	return (
		<div className={"fullscreen " + (render? "":"d-none")}>
			<h2>Loading</h2>
		</div>
	);
}