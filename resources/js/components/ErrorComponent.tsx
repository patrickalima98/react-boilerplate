import * as React from 'react';

export default function ErrorComponent ({error}) {
	return (
		<div className={"fullscreen " + (error? "":"d-none")}>
			<h2>{error.toString()}</h2>
		</div>
	);
}