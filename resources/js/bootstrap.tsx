/**
* First we will start a global window interface so we may add functions unto it
**/

interface Window {
	url 	(value:string):		string,
	asset	(value:string):		string,
	img		(value:string):		string,
	file	(value:string): 	string,
	system: 	Object,
	setSystem:	Object,
	app: 		Object,
	setApp: 	Object,
	string_url: string,
}

/**
* Some helper functions and utilities to help with the integration of some packages
**/

window.url = function (value : string = "") : string {
    return window.string_url + "/" + value;
}

window.asset = function (value : string = "") : string {
    return window.string_url + "/storage/" + value;
}

window.img = function (value : string = "") : string {
    return window.asset("img/" + value);
}

window.file = function (value : string = "") : string {
    return window.asset("file/" + value);
}