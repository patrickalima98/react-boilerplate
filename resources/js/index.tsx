
/**
 * We are manually binding the app url here.
 */
export default window.string_url = "http://localhost";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

import 						 './bootstrap';
import System           from './modules/System';

/**
 * Lastly we will have to render the application on the specified
 * element place.
 */
import * as React 		from 'react';
import * as ReactDOM    from 'react-dom';

ReactDOM.render(<System {...{}} />, document.getElementById('app') as HTMLElement);
