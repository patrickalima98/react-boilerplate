let mix 			= require('laravel-mix');
const webpack 		= require('webpack');
require('laravel-mix-bundle-analyzer');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

//Compilable files
mix.react('resources/js/index.tsx',		'public/js');
mix.sass('resources/sass/index.scss', 	'public/css');

//General

//Production settings
if (mix.inProduction()) {
    mix.version();
    mix.disableNotifications();
}
else {
	mix.sourceMaps();
	//mix.bundleAnalyzer();
}

//Separate sources into different files
mix.extract();

//Webpack configuration
mix.webpackConfig({
	resolve:  {
		modules: [
			path.resolve("./node_modules"),
			path.resolve("./resources/js/modules"),
			path.resolve("./resources/js/components"),
			path.resolve("./resources/js"),
			path.resolve("./public/fonts")
		],
		extensions: ["*", ".js", ".jsx", ".react", ".ts", ".tsx"]
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: "ts-loader",
				exclude: /node_modules/
			}
		]
	},
	devtool: "inline-source-map",
	plugins: [
	    new webpack.ContextReplacementPlugin(
	      /moment[\/\\]locale/,
	      // A regular expression matching files that should be included
	      /(pt-br)\.js/
	    ),
	    new webpack.NormalModuleReplacementPlugin(
		    /data[\\\/]packed[\\\/]latest.json$/,
		    __dirname + '/resources/js/timezone.json'
		)
  	]
});
