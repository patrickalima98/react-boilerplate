(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/public/js/index"],{

/***/ "./resources/js/bootstrap.tsx":
/*!************************************!*\
  !*** ./resources/js/bootstrap.tsx ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
* First we will start a global window interface so we may add functions unto it
**/
/**
* Some helper functions and utilities to help with the integration of some packages
**/
window.url = function (value = "") {
    return window.string_url + "/" + value;
};
window.asset = function (value = "") {
    return window.string_url + "/storage/" + value;
};
window.img = function (value = "") {
    return window.asset("img/" + value);
};
window.file = function (value = "") {
    return window.asset("file/" + value);
};


/***/ }),

/***/ "./resources/js/components/ErrorComponent.tsx":
/*!****************************************************!*\
  !*** ./resources/js/components/ErrorComponent.tsx ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ErrorComponent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function ErrorComponent({ error }) {
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "fullscreen " + (error ? "" : "d-none") },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h2", null, error.toString())));
}


/***/ }),

/***/ "./resources/js/components/LoadingComponent.tsx":
/*!******************************************************!*\
  !*** ./resources/js/components/LoadingComponent.tsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LoadingComponent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function LoadingComponent({ render }) {
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "fullscreen " + (render ? "" : "d-none") },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h2", null, "Loading")));
}


/***/ }),

/***/ "./resources/js/components/WaitingComponent.tsx":
/*!******************************************************!*\
  !*** ./resources/js/components/WaitingComponent.tsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return WaitingComponent; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function WaitingComponent({ render }) {
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", { className: "fullscreen " + (render ? "" : "d-none") },
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h2", null, "Waiting")));
}


/***/ }),

/***/ "./resources/js/index.tsx":
/*!********************************!*\
  !*** ./resources/js/index.tsx ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.tsx");
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_bootstrap__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_System__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/System */ "./resources/js/modules/System.tsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_3__);
/**
 * We are manually binding the app url here.
 */
/* harmony default export */ __webpack_exports__["default"] = (window.string_url = "http://localhost");
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */


/**
 * Lastly we will have to render the application on the specified
 * element place.
 */


react_dom__WEBPACK_IMPORTED_MODULE_3__["render"](react__WEBPACK_IMPORTED_MODULE_2__["createElement"](_modules_System__WEBPACK_IMPORTED_MODULE_1__["default"], Object.assign({}, {})), document.getElementById('app'));


/***/ }),

/***/ "./resources/js/modules/App.tsx":
/*!**************************************!*\
  !*** ./resources/js/modules/App.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return App; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ForMe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ForMe */ "./resources/js/modules/ForMe.tsx");
//Core

//Libraries

function App(props) {
    //We are going to bind a system/application state to everything
    var [app, setApp] = react__WEBPACK_IMPORTED_MODULE_0__["useState"]({});
    //Bind to window object
    window.system = app;
    window.setSystem = setApp;
    //React's render responsible into turning virtual dom into dom
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ForMe__WEBPACK_IMPORTED_MODULE_1__["Form"], null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ForMe__WEBPACK_IMPORTED_MODULE_1__["Group"], { name: "user" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ForMe__WEBPACK_IMPORTED_MODULE_1__["Input"], { name: "email" }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ForMe__WEBPACK_IMPORTED_MODULE_1__["Input"], { name: "name" })),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ForMe__WEBPACK_IMPORTED_MODULE_1__["Input"], { name: "message" }))));
}


/***/ }),

/***/ "./resources/js/modules/ForMe.tsx":
/*!****************************************!*\
  !*** ./resources/js/modules/ForMe.tsx ***!
  \****************************************/
/*! exports provided: Form, Group, Input */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Form", function() { return Form; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Group", function() { return Group; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Input", function() { return Input; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
//Core

//Standarlized context object
const GroupContext = react__WEBPACK_IMPORTED_MODULE_0__["createContext"]("");
//This will wrap all the ForMe data and transpose it to the inputs
function Form(props) {
    var [form, setForm] = react__WEBPACK_IMPORTED_MODULE_0__["useState"]({});
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("form", null,
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"](GroupContext.Provider, { value: "" }, props.children)));
}
//Group inputs so they may be an object
function Group({ children, name }) {
    //Get the positioning of the current group
    let context = react__WEBPACK_IMPORTED_MODULE_0__["useContext"](GroupContext);
    //update the context
    context = context == "" ? name : (context + "/" + name);
    //Just pass the children
    return children;
}
//Responsible for setting and saving data
function Input(props) {
    //get the input final position
    let context = react__WEBPACK_IMPORTED_MODULE_0__["useContext"](GroupContext);
    console.log(context);
    return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", null));
}


/***/ }),

/***/ "./resources/js/modules/System.tsx":
/*!*****************************************!*\
  !*** ./resources/js/modules/System.tsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return System; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App */ "./resources/js/modules/App.tsx");
/* harmony import */ var _SystemData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./SystemData */ "./resources/js/modules/SystemData.tsx");
/* harmony import */ var _components_ErrorComponent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/ErrorComponent */ "./resources/js/components/ErrorComponent.tsx");
/* harmony import */ var _components_LoadingComponent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/LoadingComponent */ "./resources/js/components/LoadingComponent.tsx");
/* harmony import */ var _components_WaitingComponent__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/WaitingComponent */ "./resources/js/components/WaitingComponent.tsx");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-toastify */ "./node_modules/react-toastify/esm/react-toastify.js");
//Core


//Modules


//Components



//Packages

//Class responsible for handling everything under the hood for our application to work
class System extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
    constructor() {
        super(...arguments);
        this.state = {
            user: null,
            error: null,
            loading: true,
            waiting: true,
            redirected: false,
            redirecturl: "",
            token: "",
        };
    }
    //-------------------------------------------------
    // Main functions
    //-------------------------------------------------
    //React's render method responsible into turning virtual dom into dom
    render() {
        //Render redirection
        if (this.state.redirecturl && !this.state.redirected) {
            return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["BrowserRouter"], { basename: "/" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], { to: this.state.redirecturl })));
        }
        //Render error component
        if (this.state.error) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_ErrorComponent__WEBPACK_IMPORTED_MODULE_4__["default"], { error: this.state.error });
        }
        //Render normal component
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_router_dom__WEBPACK_IMPORTED_MODULE_1__["BrowserRouter"], { basename: "/" },
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_SystemData__WEBPACK_IMPORTED_MODULE_3__["default"], { object: this },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_App__WEBPACK_IMPORTED_MODULE_2__["default"], null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_toastify__WEBPACK_IMPORTED_MODULE_7__["ToastContainer"], { position: react_toastify__WEBPACK_IMPORTED_MODULE_7__["toast"].POSITION.BOTTOM_RIGHT }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_LoadingComponent__WEBPACK_IMPORTED_MODULE_5__["default"], { render: this.state.loading }),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_WaitingComponent__WEBPACK_IMPORTED_MODULE_6__["default"], { render: this.state.waiting }))));
    }
    //-------------------------------------------------
    // Fetch functions
    //-------------------------------------------------
    //Simple fetch that doesn't require auth
    fetch(target, options, onSuccess = null, onError = null) {
        //Is the system waiting?
        if (this.state.waiting)
            return;
        //Gather special options
        const { wait, toastify } = options;
        //Disable the page while it loads
        if (wait)
            this.setState({ waiting: true });
        //The API request
        return fetch(window.url(target), options).
            then(res => {
            if (!res.ok && res.status == 500) {
                this.setState({ error: res.statusText });
            }
            else if (!res.ok && res.status == 401) {
                sessionStorage.removeItem("token");
                //Set state
                this.setState({
                    token: null,
                    user: null,
                });
            }
            //Reenable the page after it loads
            if (wait)
                this.setState({ waiting: false });
            return res;
        }).
            then(res => res.json()).
            then(response => {
            //Returned a non fatal error from the server
            if (response.error && toastify) {
                if (!Array.isArray(response.error) && !(typeof response.error === 'object')) {
                    react_toastify__WEBPACK_IMPORTED_MODULE_7__["toast"].warn(response.error);
                }
                else {
                    for (let index in response.error) {
                        react_toastify__WEBPACK_IMPORTED_MODULE_7__["toast"].warn(response.error[index][0]);
                    }
                }
                if (onError)
                    onError(response);
            }
            //Everything went right
            else if (response.success && toastify) {
                if (!Array.isArray(response.success) && !(typeof response.success === 'object')) {
                    react_toastify__WEBPACK_IMPORTED_MODULE_7__["toast"].success(response.success);
                }
                else {
                    for (let index in response.success) {
                        react_toastify__WEBPACK_IMPORTED_MODULE_7__["toast"].success(response.success[index]);
                    }
                }
                if (onSuccess)
                    onSuccess(response);
            }
            //No feedback given, just consider successfull
            else if (onSuccess) {
                onSuccess(response);
            }
        });
    }
    //Elaborated fetch that handles auth
    api(target, options, onSuccess = null, onError = null) {
        let preheader = {
            headers: {
                "Authorization": "Bearer " + this.state.token,
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            wait: true,
            toastify: true
        };
        return this.fetch("api/" + target, Object.assign(preheader, options), onSuccess, onError);
    }
    //-------------------------------------------------
    // Redirect functions
    //-------------------------------------------------
    redirect(url = "/") {
        this.setState({
            redirecturl: url,
            redirected: false,
        });
    }
    //-------------------------------------------------
    // Error functions
    //-------------------------------------------------
    //Will display an error and prevent the react from crashing
    error(error) {
        this.setState({ error });
    }
    componentDidCatch(error, info) {
        this.setState({ error });
    }
}


/***/ }),

/***/ "./resources/js/modules/SystemData.tsx":
/*!*********************************************!*\
  !*** ./resources/js/modules/SystemData.tsx ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return System; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

//This will be responsible for handling global system calls and foward it to the class
function System({ object, children }) {
    //We are going to bind a system/application state to everything
    var [system, setSystem] = react__WEBPACK_IMPORTED_MODULE_0__["useState"]({
        fetch: object.fetch,
        api: object.api,
        redirect: object.redirect,
        error: object.error,
        user: object.state.user
    });
    //Bind to window object
    window.system = system;
    window.setSystem = setSystem;
    //Force a null response
    return children;
}


/***/ }),

/***/ "./resources/sass/index.scss":
/*!***********************************!*\
  !*** ./resources/sass/index.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!******************************************************************!*\
  !*** multi ./resources/js/index.tsx ./resources/sass/index.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\wamp64\www\react-boilerplate\resources\js\index.tsx */"./resources/js/index.tsx");
module.exports = __webpack_require__(/*! C:\wamp64\www\react-boilerplate\resources\sass\index.scss */"./resources/sass/index.scss");


/***/ })

},[[0,"/public/js/manifest","/public/js/vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvYm9vdHN0cmFwLnRzeCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvY29tcG9uZW50cy9FcnJvckNvbXBvbmVudC50c3giLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2NvbXBvbmVudHMvTG9hZGluZ0NvbXBvbmVudC50c3giLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2NvbXBvbmVudHMvV2FpdGluZ0NvbXBvbmVudC50c3giLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL2luZGV4LnRzeCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9BcHAudHN4Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9tb2R1bGVzL0Zvck1lLnRzeCIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9TeXN0ZW0udHN4Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy9tb2R1bGVzL1N5c3RlbURhdGEudHN4Iiwid2VicGFjazovLy8uL3Jlc291cmNlcy9zYXNzL2luZGV4LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0dBRUc7QUFjSDs7R0FFRztBQUVILE1BQU0sQ0FBQyxHQUFHLEdBQUcsVUFBVSxRQUFpQixFQUFFO0lBQ3RDLE9BQU8sTUFBTSxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDO0FBQzNDLENBQUM7QUFFRCxNQUFNLENBQUMsS0FBSyxHQUFHLFVBQVUsUUFBaUIsRUFBRTtJQUN4QyxPQUFPLE1BQU0sQ0FBQyxVQUFVLEdBQUcsV0FBVyxHQUFHLEtBQUssQ0FBQztBQUNuRCxDQUFDO0FBRUQsTUFBTSxDQUFDLEdBQUcsR0FBRyxVQUFVLFFBQWlCLEVBQUU7SUFDdEMsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQztBQUN4QyxDQUFDO0FBRUQsTUFBTSxDQUFDLElBQUksR0FBRyxVQUFVLFFBQWlCLEVBQUU7SUFDdkMsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsQ0FBQztBQUN6QyxDQUFDOzs7Ozs7Ozs7Ozs7O0FDbENEO0FBQUE7QUFBQTtBQUFBO0FBQStCO0FBRWhCLFNBQVMsY0FBYyxDQUFFLEVBQUMsS0FBSyxFQUFDO0lBQzlDLE9BQU8sQ0FDTiw2REFBSyxTQUFTLEVBQUUsYUFBYSxHQUFHLENBQUMsS0FBSyxFQUFDLENBQUMsRUFBRSxFQUFDLFNBQVEsQ0FBQztRQUNuRCxnRUFBSyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQU0sQ0FDdEIsQ0FDTixDQUFDO0FBQ0gsQ0FBQzs7Ozs7Ozs7Ozs7OztBQ1JEO0FBQUE7QUFBQTtBQUFBO0FBQStCO0FBRWhCLFNBQVMsZ0JBQWdCLENBQUUsRUFBQyxNQUFNLEVBQUM7SUFDakQsT0FBTyxDQUNOLDZEQUFLLFNBQVMsRUFBRSxhQUFhLEdBQUcsQ0FBQyxNQUFNLEVBQUMsQ0FBQyxFQUFFLEVBQUMsU0FBUSxDQUFDO1FBQ3BELDBFQUFnQixDQUNYLENBQ04sQ0FBQztBQUNILENBQUM7Ozs7Ozs7Ozs7Ozs7QUNSRDtBQUFBO0FBQUE7QUFBQTtBQUErQjtBQUVoQixTQUFTLGdCQUFnQixDQUFFLEVBQUMsTUFBTSxFQUFDO0lBQ2pELE9BQU8sQ0FDTiw2REFBSyxTQUFTLEVBQUUsYUFBYSxHQUFHLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLFNBQVEsQ0FBQztRQUNwRCwwRUFBZ0IsQ0FDWCxDQUNOLENBQUM7QUFDSCxDQUFDOzs7Ozs7Ozs7Ozs7O0FDUEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOztHQUVHO0FBQ1kscUVBQU0sQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLEVBQUM7QUFFdEQ7Ozs7R0FJRztBQUV5QjtBQUNvQjtBQUVoRDs7O0dBR0c7QUFDOEI7QUFDUTtBQUV6QyxnREFBZSxDQUFDLG9EQUFDLHVEQUFNLG9CQUFLLEVBQUUsRUFBSSxFQUFFLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFnQixDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7QUN0Qm5GO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQUFNO0FBQ3NDO0FBRzVDLFdBQVc7QUFDb0M7QUFFaEMsU0FBUyxHQUFHLENBQUUsS0FBSztJQUU5QiwrREFBK0Q7SUFDL0QsSUFBSSxDQUFFLEdBQUcsRUFBRSxNQUFNLENBQUUsR0FBRyw4Q0FBYyxDQUFDLEVBRXBDLENBQUMsQ0FBQztJQUVILHVCQUF1QjtJQUN2QixNQUFNLENBQUMsTUFBTSxHQUFPLEdBQUcsQ0FBQztJQUN4QixNQUFNLENBQUMsU0FBUyxHQUFJLE1BQU0sQ0FBQztJQUUzQiw4REFBOEQ7SUFDOUQsT0FBTyxDQUNILG9EQUFDLDhDQUFjO1FBQ1gsb0RBQUMsMkNBQUk7WUFDRCxvREFBQyw0Q0FBSyxJQUFDLElBQUksRUFBQyxNQUFNO2dCQUNkLG9EQUFDLDRDQUFLLElBQUMsSUFBSSxFQUFDLE9BQU8sR0FBRztnQkFDdEIsb0RBQUMsNENBQUssSUFBQyxJQUFJLEVBQUMsTUFBTSxHQUFJLENBQ2xCO1lBRVIsb0RBQUMsNENBQUssSUFBQyxJQUFJLEVBQUMsU0FBUyxHQUFHLENBQ3JCLENBQ00sQ0FDcEIsQ0FBQztBQUNOLENBQUM7Ozs7Ozs7Ozs7Ozs7QUMvQkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsTUFBTTtBQUN5QjtBQUUvQiw2QkFBNkI7QUFDN0IsTUFBTSxZQUFZLEdBQUcsbURBQW1CLENBQUMsRUFBRSxDQUFDLENBQUM7QUFFN0Msa0VBQWtFO0FBQzNELFNBQVMsSUFBSSxDQUFFLEtBQUs7SUFDMUIsSUFBSSxDQUFFLElBQUksRUFBRSxPQUFPLENBQUUsR0FBRyw4Q0FBYyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRTNDLE9BQU8sQ0FDTjtRQUNDLG9EQUFDLFlBQVksQ0FBQyxRQUFRLElBQUMsS0FBSyxFQUFFLEVBQUUsSUFDOUIsS0FBSyxDQUFDLFFBQVEsQ0FDUSxDQUNsQixDQUNQLENBQUM7QUFDSCxDQUFDO0FBRUQsdUNBQXVDO0FBQ2hDLFNBQVMsS0FBSyxDQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQztJQUN0QywwQ0FBMEM7SUFDMUMsSUFBSSxPQUFPLEdBQUcsZ0RBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFFN0Msb0JBQW9CO0lBQ3BCLE9BQU8sR0FBRyxPQUFPLElBQUksRUFBRSxFQUFDLENBQUMsSUFBSSxFQUFDLEVBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUVyRCx3QkFBd0I7SUFDeEIsT0FBTyxRQUFRLENBQUM7QUFDakIsQ0FBQztBQUVELHlDQUF5QztBQUNsQyxTQUFTLEtBQUssQ0FBRSxLQUFLO0lBQzNCLDhCQUE4QjtJQUM5QixJQUFJLE9BQU8sR0FBRyxnREFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUU3QyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBRXJCLE9BQU8sQ0FDTixrRUFFRSxDQUNGLENBQUM7QUFDSCxDQUFDOzs7Ozs7Ozs7Ozs7O0FDM0NEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQUFNO0FBQzJDO0FBQ1c7QUFFNUQsU0FBUztBQUM2QztBQUNNO0FBRTVELFlBQVk7QUFDZ0U7QUFDRTtBQUNBO0FBRTlFLFVBQVU7QUFDb0Q7QUFFOUQsc0ZBQXNGO0FBQ3ZFLE1BQU0sTUFBTyxTQUFRLCtDQUFlO0lBQW5EOztRQUVJLFVBQUssR0FBRztZQUNKLElBQUksRUFBWSxJQUFJO1lBQ3BCLEtBQUssRUFBVyxJQUFJO1lBQ3BCLE9BQU8sRUFBUyxJQUFJO1lBQ3BCLE9BQU8sRUFBUyxJQUFJO1lBQ3BCLFVBQVUsRUFBTSxLQUFLO1lBQ3JCLFdBQVcsRUFBSyxFQUFFO1lBQ2xCLEtBQUssRUFBVyxFQUFFO1NBQ3JCO0lBb0pMLENBQUM7SUFsSkcsbURBQW1EO0lBQ25ELGlCQUFpQjtJQUNqQixtREFBbUQ7SUFFbkQscUVBQXFFO0lBQzlELE1BQU07UUFDVCxvQkFBb0I7UUFDcEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFO1lBQ2xELE9BQU8sQ0FDSCxvREFBQyw4REFBYSxJQUFDLFFBQVEsRUFBRSxHQUFHO2dCQUN4QixvREFBQyx5REFBUSxJQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBSSxDQUM1QixDQUNuQixDQUFDO1NBQ0w7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNsQixPQUFPLG9EQUFDLGtFQUFjLElBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFJLENBQUM7U0FDdEQ7UUFFRCx5QkFBeUI7UUFDekIsT0FBTyxDQUNILG9EQUFDLDhEQUFhLElBQUMsUUFBUSxFQUFFLEdBQUc7WUFDeEIsb0RBQUMsbURBQVUsSUFBQyxNQUFNLEVBQUUsSUFBSTtnQkFDcEIsb0RBQUMsNENBQUcsT0FBRTtnQkFDTixvREFBQyw2REFBYyxJQUFDLFFBQVEsRUFBRSxvREFBSyxDQUFDLFFBQVEsQ0FBQyxZQUFZLEdBQUk7Z0JBQ3pELG9EQUFDLG9FQUFnQixJQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBSTtnQkFDaEQsb0RBQUMsb0VBQWdCLElBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFJLENBQ3ZDLENBQ0QsQ0FDbkIsQ0FBQztJQUNOLENBQUM7SUFFRCxtREFBbUQ7SUFDbkQsa0JBQWtCO0lBQ2xCLG1EQUFtRDtJQUVuRCx3Q0FBd0M7SUFDeEMsS0FBSyxDQUFFLE1BQVcsRUFBRSxPQUFXLEVBQUUsWUFBaUIsSUFBSSxFQUFFLFVBQWUsSUFBSTtRQUN2RSx3QkFBd0I7UUFDeEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87WUFBRSxPQUFPO1FBRS9CLHdCQUF3QjtRQUN4QixNQUFNLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLE9BQU8sQ0FBQztRQUVuQyxpQ0FBaUM7UUFDakMsSUFBSSxJQUFJO1lBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLE9BQU8sRUFBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO1FBRXhDLGlCQUFpQjtRQUNqQixPQUFPLEtBQUssQ0FBRSxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU8sQ0FBQztZQUMxQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDUCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLEtBQUssRUFBQyxHQUFHLENBQUMsVUFBVSxFQUFDLENBQUMsQ0FBQzthQUN6QztpQkFDSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQkFDbkMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFFbkMsV0FBVztnQkFDWCxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUNWLEtBQUssRUFBRyxJQUFJO29CQUNaLElBQUksRUFBSSxJQUFJO2lCQUNmLENBQUM7YUFDTDtZQUVELGtDQUFrQztZQUNsQyxJQUFJLElBQUk7Z0JBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO1lBRXpDLE9BQU8sR0FBRyxDQUFDO1FBQ2YsQ0FBQyxDQUFDO1lBQ0YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNaLDRDQUE0QztZQUM1QyxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO2dCQUU1QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sUUFBUSxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsRUFBRTtvQkFDekUsb0RBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM5QjtxQkFDSTtvQkFFRCxLQUFLLElBQUksS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLEVBQUU7d0JBQzlCLG9EQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDeEM7aUJBQ0o7Z0JBRUQsSUFBSSxPQUFPO29CQUFFLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNsQztZQUVELHVCQUF1QjtpQkFDbEIsSUFBSSxRQUFRLENBQUMsT0FBTyxJQUFJLFFBQVEsRUFBRTtnQkFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLFFBQVEsQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLEVBQUU7b0JBQzdFLG9EQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDbkM7cUJBQ0k7b0JBQ0QsS0FBSyxJQUFJLEtBQUssSUFBSSxRQUFRLENBQUMsT0FBTyxFQUFFO3dCQUNoQyxvREFBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7cUJBQzFDO2lCQUNKO2dCQUVELElBQUksU0FBUztvQkFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDdEM7WUFFRCw4Q0FBOEM7aUJBQ3pDLElBQUksU0FBUyxFQUFFO2dCQUNoQixTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQ0FBb0M7SUFDcEMsR0FBRyxDQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsU0FBUyxHQUFHLElBQUksRUFBRSxPQUFPLEdBQUcsSUFBSTtRQUNsRCxJQUFJLFNBQVMsR0FBRztZQUNaLE9BQU8sRUFBRTtnQkFDTCxlQUFlLEVBQUcsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSztnQkFDOUMsY0FBYyxFQUFJLGtCQUFrQjtnQkFDcEMsUUFBUSxFQUFVLGtCQUFrQjthQUN2QztZQUNELElBQUksRUFBQyxJQUFJO1lBQ1QsUUFBUSxFQUFDLElBQUk7U0FDaEIsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQsbURBQW1EO0lBQ25ELHFCQUFxQjtJQUNyQixtREFBbUQ7SUFFbkQsUUFBUSxDQUFFLEdBQUcsR0FBRyxHQUFHO1FBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNWLFdBQVcsRUFBSyxHQUFHO1lBQ25CLFVBQVUsRUFBTSxLQUFLO1NBQ3hCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtREFBbUQ7SUFDbkQsa0JBQWtCO0lBQ2xCLG1EQUFtRDtJQUVuRCwyREFBMkQ7SUFDM0QsS0FBSyxDQUFFLEtBQUs7UUFDUixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSyxFQUFFLElBQUk7UUFDekIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztDQUNKOzs7Ozs7Ozs7Ozs7O0FDL0tEO0FBQUE7QUFBQTtBQUFBO0FBQStCO0FBRS9CLHNGQUFzRjtBQUN2RSxTQUFTLE1BQU0sQ0FBRSxFQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUM7SUFFOUMsK0RBQStEO0lBQy9ELElBQUksQ0FBRSxNQUFNLEVBQUUsU0FBUyxDQUFFLEdBQUcsOENBQWMsQ0FBQztRQUN2QyxLQUFLLEVBQUssTUFBTSxDQUFDLEtBQUs7UUFDdEIsR0FBRyxFQUFPLE1BQU0sQ0FBQyxHQUFHO1FBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTtRQUN6QixLQUFLLEVBQUssTUFBTSxDQUFDLEtBQUs7UUFDdEIsSUFBSSxFQUFNLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSTtLQUM5QixDQUFDLENBQUM7SUFFSCx1QkFBdUI7SUFDdkIsTUFBTSxDQUFDLE1BQU0sR0FBTyxNQUFNLENBQUM7SUFDM0IsTUFBTSxDQUFDLFNBQVMsR0FBSSxTQUFTLENBQUM7SUFFOUIsdUJBQXVCO0lBQ3ZCLE9BQU8sUUFBUSxDQUFDO0FBQ3BCLENBQUM7Ozs7Ozs7Ozs7OztBQ3BCRCx5QyIsImZpbGUiOiIvcHVibGljL2pzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiogRmlyc3Qgd2Ugd2lsbCBzdGFydCBhIGdsb2JhbCB3aW5kb3cgaW50ZXJmYWNlIHNvIHdlIG1heSBhZGQgZnVuY3Rpb25zIHVudG8gaXRcclxuKiovXHJcblxyXG5pbnRlcmZhY2UgV2luZG93IHtcclxuXHR1cmwgXHQodmFsdWU6c3RyaW5nKTpcdFx0c3RyaW5nLFxyXG5cdGFzc2V0XHQodmFsdWU6c3RyaW5nKTpcdFx0c3RyaW5nLFxyXG5cdGltZ1x0XHQodmFsdWU6c3RyaW5nKTpcdFx0c3RyaW5nLFxyXG5cdGZpbGVcdCh2YWx1ZTpzdHJpbmcpOiBcdHN0cmluZyxcclxuXHRzeXN0ZW06IFx0T2JqZWN0LFxyXG5cdHNldFN5c3RlbTpcdE9iamVjdCxcclxuXHRhcHA6IFx0XHRPYmplY3QsXHJcblx0c2V0QXBwOiBcdE9iamVjdCxcclxuXHRzdHJpbmdfdXJsOiBzdHJpbmcsXHJcbn1cclxuXHJcbi8qKlxyXG4qIFNvbWUgaGVscGVyIGZ1bmN0aW9ucyBhbmQgdXRpbGl0aWVzIHRvIGhlbHAgd2l0aCB0aGUgaW50ZWdyYXRpb24gb2Ygc29tZSBwYWNrYWdlc1xyXG4qKi9cclxuXHJcbndpbmRvdy51cmwgPSBmdW5jdGlvbiAodmFsdWUgOiBzdHJpbmcgPSBcIlwiKSA6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gd2luZG93LnN0cmluZ191cmwgKyBcIi9cIiArIHZhbHVlO1xyXG59XHJcblxyXG53aW5kb3cuYXNzZXQgPSBmdW5jdGlvbiAodmFsdWUgOiBzdHJpbmcgPSBcIlwiKSA6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gd2luZG93LnN0cmluZ191cmwgKyBcIi9zdG9yYWdlL1wiICsgdmFsdWU7XHJcbn1cclxuXHJcbndpbmRvdy5pbWcgPSBmdW5jdGlvbiAodmFsdWUgOiBzdHJpbmcgPSBcIlwiKSA6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gd2luZG93LmFzc2V0KFwiaW1nL1wiICsgdmFsdWUpO1xyXG59XHJcblxyXG53aW5kb3cuZmlsZSA9IGZ1bmN0aW9uICh2YWx1ZSA6IHN0cmluZyA9IFwiXCIpIDogc3RyaW5nIHtcclxuICAgIHJldHVybiB3aW5kb3cuYXNzZXQoXCJmaWxlL1wiICsgdmFsdWUpO1xyXG59IiwiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gRXJyb3JDb21wb25lbnQgKHtlcnJvcn0pIHtcclxuXHRyZXR1cm4gKFxyXG5cdFx0PGRpdiBjbGFzc05hbWU9e1wiZnVsbHNjcmVlbiBcIiArIChlcnJvcj8gXCJcIjpcImQtbm9uZVwiKX0+XHJcblx0XHRcdDxoMj57ZXJyb3IudG9TdHJpbmcoKX08L2gyPlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufSIsImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIExvYWRpbmdDb21wb25lbnQgKHtyZW5kZXJ9KSB7XHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lPXtcImZ1bGxzY3JlZW4gXCIgKyAocmVuZGVyPyBcIlwiOlwiZC1ub25lXCIpfT5cclxuXHRcdFx0PGgyPkxvYWRpbmc8L2gyPlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufSIsImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFdhaXRpbmdDb21wb25lbnQgKHtyZW5kZXJ9KSB7XHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lPXtcImZ1bGxzY3JlZW4gXCIgKyAocmVuZGVyPyBcIlwiOlwiZC1ub25lXCIpfT5cclxuXHRcdFx0PGgyPldhaXRpbmc8L2gyPlxyXG5cdFx0PC9kaXY+XHJcblx0KTtcclxufSIsIlxyXG4vKipcclxuICogV2UgYXJlIG1hbnVhbGx5IGJpbmRpbmcgdGhlIGFwcCB1cmwgaGVyZS5cclxuICovXHJcbmV4cG9ydCBkZWZhdWx0IHdpbmRvdy5zdHJpbmdfdXJsID0gXCJodHRwOi8vbG9jYWxob3N0XCI7XHJcblxyXG4vKipcclxuICogRmlyc3Qgd2Ugd2lsbCBsb2FkIGFsbCBvZiB0aGlzIHByb2plY3QncyBKYXZhU2NyaXB0IGRlcGVuZGVuY2llcyB3aGljaFxyXG4gKiBpbmNsdWRlcyBSZWFjdCBhbmQgb3RoZXIgaGVscGVycy4gSXQncyBhIGdyZWF0IHN0YXJ0aW5nIHBvaW50IHdoaWxlXHJcbiAqIGJ1aWxkaW5nIHJvYnVzdCwgcG93ZXJmdWwgd2ViIGFwcGxpY2F0aW9ucyB1c2luZyBSZWFjdCArIExhcmF2ZWwuXHJcbiAqL1xyXG5cclxuaW1wb3J0IFx0XHRcdFx0XHRcdCAnLi9ib290c3RyYXAnO1xyXG5pbXBvcnQgU3lzdGVtICAgICAgICAgICBmcm9tICcuL21vZHVsZXMvU3lzdGVtJztcclxuXHJcbi8qKlxyXG4gKiBMYXN0bHkgd2Ugd2lsbCBoYXZlIHRvIHJlbmRlciB0aGUgYXBwbGljYXRpb24gb24gdGhlIHNwZWNpZmllZFxyXG4gKiBlbGVtZW50IHBsYWNlLlxyXG4gKi9cclxuaW1wb3J0ICogYXMgUmVhY3QgXHRcdGZyb20gJ3JlYWN0JztcclxuaW1wb3J0ICogYXMgUmVhY3RET00gICAgZnJvbSAncmVhY3QtZG9tJztcclxuXHJcblJlYWN0RE9NLnJlbmRlcig8U3lzdGVtIHsuLi57fX0gLz4sIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhcHAnKSBhcyBIVE1MRWxlbWVudCk7XHJcbiIsIi8vQ29yZVxyXG5pbXBvcnQgKiBhcyBSZWFjdCBcdCAgICAgICAgICAgIGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHtTd2l0Y2h9ICAgICAgICAgICAgICAgICBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xyXG5cclxuLy9MaWJyYXJpZXNcclxuaW1wb3J0IHtGb3JtLCBHcm91cCwgSW5wdXR9ICAgICBmcm9tIFwiLi9Gb3JNZVwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQXBwIChwcm9wcykge1xyXG5cclxuICAgIC8vV2UgYXJlIGdvaW5nIHRvIGJpbmQgYSBzeXN0ZW0vYXBwbGljYXRpb24gc3RhdGUgdG8gZXZlcnl0aGluZ1xyXG4gICAgdmFyIFsgYXBwLCBzZXRBcHAgXSA9IFJlYWN0LnVzZVN0YXRlKHtcclxuXHJcbiAgICB9KTtcclxuXHJcbiAgICAvL0JpbmQgdG8gd2luZG93IG9iamVjdFxyXG4gICAgd2luZG93LnN5c3RlbSAgICAgPSBhcHA7XHJcbiAgICB3aW5kb3cuc2V0U3lzdGVtICA9IHNldEFwcDtcclxuXHJcbiAgICAvL1JlYWN0J3MgcmVuZGVyIHJlc3BvbnNpYmxlIGludG8gdHVybmluZyB2aXJ0dWFsIGRvbSBpbnRvIGRvbVxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgICAgICAgIDxGb3JtPlxyXG4gICAgICAgICAgICAgICAgPEdyb3VwIG5hbWU9XCJ1c2VyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPElucHV0IG5hbWU9XCJlbWFpbFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPElucHV0IG5hbWU9XCJuYW1lXCIgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0dyb3VwPlxyXG5cclxuICAgICAgICAgICAgICAgIDxJbnB1dCBuYW1lPVwibWVzc2FnZVwiIC8+XHJcbiAgICAgICAgICAgIDwvRm9ybT5cclxuICAgICAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICAgKTtcclxufSIsIi8vQ29yZVxyXG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XHJcblxyXG4vL1N0YW5kYXJsaXplZCBjb250ZXh0IG9iamVjdFxyXG5jb25zdCBHcm91cENvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0KFwiXCIpO1xyXG5cclxuLy9UaGlzIHdpbGwgd3JhcCBhbGwgdGhlIEZvck1lIGRhdGEgYW5kIHRyYW5zcG9zZSBpdCB0byB0aGUgaW5wdXRzXHJcbmV4cG9ydCBmdW5jdGlvbiBGb3JtIChwcm9wcykge1xyXG5cdHZhciBbIGZvcm0sIHNldEZvcm0gXSA9IFJlYWN0LnVzZVN0YXRlKHt9KTtcclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxmb3JtPlxyXG5cdFx0XHQ8R3JvdXBDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXtcIlwifT5cclxuXHRcdFx0XHR7cHJvcHMuY2hpbGRyZW59XHJcblx0XHRcdDwvR3JvdXBDb250ZXh0LlByb3ZpZGVyPlxyXG5cdFx0PC9mb3JtPlxyXG5cdCk7XHJcbn1cclxuXHJcbi8vR3JvdXAgaW5wdXRzIHNvIHRoZXkgbWF5IGJlIGFuIG9iamVjdFxyXG5leHBvcnQgZnVuY3Rpb24gR3JvdXAgKHtjaGlsZHJlbiwgbmFtZX0pIHtcclxuXHQvL0dldCB0aGUgcG9zaXRpb25pbmcgb2YgdGhlIGN1cnJlbnQgZ3JvdXBcclxuXHRsZXQgY29udGV4dCA9IFJlYWN0LnVzZUNvbnRleHQoR3JvdXBDb250ZXh0KTtcclxuXHJcblx0Ly91cGRhdGUgdGhlIGNvbnRleHRcclxuXHRjb250ZXh0ID0gY29udGV4dCA9PSBcIlwiPyBuYW1lOihjb250ZXh0ICsgXCIvXCIgKyBuYW1lKTtcclxuXHJcblx0Ly9KdXN0IHBhc3MgdGhlIGNoaWxkcmVuXHJcblx0cmV0dXJuIGNoaWxkcmVuO1xyXG59XHJcblxyXG4vL1Jlc3BvbnNpYmxlIGZvciBzZXR0aW5nIGFuZCBzYXZpbmcgZGF0YVxyXG5leHBvcnQgZnVuY3Rpb24gSW5wdXQgKHByb3BzKSB7XHJcblx0Ly9nZXQgdGhlIGlucHV0IGZpbmFsIHBvc2l0aW9uXHJcblx0bGV0IGNvbnRleHQgPSBSZWFjdC51c2VDb250ZXh0KEdyb3VwQ29udGV4dCk7XHJcblxyXG5cdGNvbnNvbGUubG9nKGNvbnRleHQpO1xyXG5cclxuXHRyZXR1cm4gKFxyXG5cdFx0PGlucHV0XHJcblxyXG5cdFx0Lz5cclxuXHQpO1xyXG59IiwiLy9Db3JlXHJcbmltcG9ydCAqIGFzIFJlYWN0ICAgICAgICAgICAgICAgICAgIGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHtSZWRpcmVjdCwgQnJvd3NlclJvdXRlcn0gICAgZnJvbSBcInJlYWN0LXJvdXRlci1kb21cIjtcclxuXHJcbi8vTW9kdWxlc1xyXG5pbXBvcnQgQXBwICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnJvbSAgXCIuL0FwcFwiO1xyXG5pbXBvcnQgU3lzdGVtRGF0YSAgICAgICAgICAgICAgICAgICAgICAgZnJvbSBcIi4vU3lzdGVtRGF0YVwiO1xyXG5cclxuLy9Db21wb25lbnRzXHJcbmltcG9ydCBFcnJvckNvbXBvbmVudCAgICAgICAgICAgICAgICAgICBmcm9tIFwiLi4vY29tcG9uZW50cy9FcnJvckNvbXBvbmVudFwiO1xyXG5pbXBvcnQgTG9hZGluZ0NvbXBvbmVudCAgICAgICAgICAgICAgICAgZnJvbSBcIi4uL2NvbXBvbmVudHMvTG9hZGluZ0NvbXBvbmVudFwiO1xyXG5pbXBvcnQgV2FpdGluZ0NvbXBvbmVudCAgICAgICAgICAgICAgICAgZnJvbSBcIi4uL2NvbXBvbmVudHMvV2FpdGluZ0NvbXBvbmVudFwiO1xyXG5cclxuLy9QYWNrYWdlc1xyXG5pbXBvcnQgeyBUb2FzdENvbnRhaW5lciwgdG9hc3QgfSAgICAgICAgZnJvbSAncmVhY3QtdG9hc3RpZnknO1xyXG5cclxuLy9DbGFzcyByZXNwb25zaWJsZSBmb3IgaGFuZGxpbmcgZXZlcnl0aGluZyB1bmRlciB0aGUgaG9vZCBmb3Igb3VyIGFwcGxpY2F0aW9uIHRvIHdvcmtcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3lzdGVtIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50ICB7XHJcblxyXG4gICAgc3RhdGUgPSB7XHJcbiAgICAgICAgdXNlcjogICAgICAgICAgIG51bGwsXHJcbiAgICAgICAgZXJyb3I6ICAgICAgICAgIG51bGwsXHJcbiAgICAgICAgbG9hZGluZzogICAgICAgIHRydWUsXHJcbiAgICAgICAgd2FpdGluZzogICAgICAgIHRydWUsXHJcbiAgICAgICAgcmVkaXJlY3RlZDogICAgIGZhbHNlLFxyXG4gICAgICAgIHJlZGlyZWN0dXJsOiAgICBcIlwiLFxyXG4gICAgICAgIHRva2VuOiAgICAgICAgICBcIlwiLFxyXG4gICAgfVxyXG5cclxuICAgIC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gTWFpbiBmdW5jdGlvbnNcclxuICAgIC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8vUmVhY3QncyByZW5kZXIgbWV0aG9kIHJlc3BvbnNpYmxlIGludG8gdHVybmluZyB2aXJ0dWFsIGRvbSBpbnRvIGRvbVxyXG4gICAgcHVibGljIHJlbmRlcigpIHtcclxuICAgICAgICAvL1JlbmRlciByZWRpcmVjdGlvblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlZGlyZWN0dXJsICYmICF0aGlzLnN0YXRlLnJlZGlyZWN0ZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxCcm93c2VyUm91dGVyIGJhc2VuYW1lPXtcIi9cIn0+XHJcbiAgICAgICAgICAgICAgICAgICAgPFJlZGlyZWN0IHRvPXt0aGlzLnN0YXRlLnJlZGlyZWN0dXJsfSAvPlxyXG4gICAgICAgICAgICAgICAgPC9Ccm93c2VyUm91dGVyPlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9SZW5kZXIgZXJyb3IgY29tcG9uZW50XHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUuZXJyb3IpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxFcnJvckNvbXBvbmVudCBlcnJvcj17dGhpcy5zdGF0ZS5lcnJvcn0gLz47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvL1JlbmRlciBub3JtYWwgY29tcG9uZW50XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJyb3dzZXJSb3V0ZXIgYmFzZW5hbWU9e1wiL1wifT5cclxuICAgICAgICAgICAgICAgIDxTeXN0ZW1EYXRhIG9iamVjdD17dGhpc30+XHJcbiAgICAgICAgICAgICAgICAgICAgPEFwcC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPFRvYXN0Q29udGFpbmVyIHBvc2l0aW9uPXt0b2FzdC5QT1NJVElPTi5CT1RUT01fUklHSFR9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPExvYWRpbmdDb21wb25lbnQgcmVuZGVyPXt0aGlzLnN0YXRlLmxvYWRpbmd9IC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPFdhaXRpbmdDb21wb25lbnQgcmVuZGVyPXt0aGlzLnN0YXRlLndhaXRpbmd9IC8+XHJcbiAgICAgICAgICAgICAgICA8L1N5c3RlbURhdGE+XHJcbiAgICAgICAgICAgIDwvQnJvd3NlclJvdXRlcj5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gRmV0Y2ggZnVuY3Rpb25zXHJcbiAgICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvL1NpbXBsZSBmZXRjaCB0aGF0IGRvZXNuJ3QgcmVxdWlyZSBhdXRoXHJcbiAgICBmZXRjaCAodGFyZ2V0OiBhbnksIG9wdGlvbnM6YW55LCBvblN1Y2Nlc3M6IGFueSA9IG51bGwsIG9uRXJyb3I6IGFueSA9IG51bGwpIHtcclxuICAgICAgICAvL0lzIHRoZSBzeXN0ZW0gd2FpdGluZz9cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS53YWl0aW5nKSByZXR1cm47XHJcblxyXG4gICAgICAgIC8vR2F0aGVyIHNwZWNpYWwgb3B0aW9uc1xyXG4gICAgICAgIGNvbnN0IHsgd2FpdCwgdG9hc3RpZnkgfSA9IG9wdGlvbnM7XHJcblxyXG4gICAgICAgIC8vRGlzYWJsZSB0aGUgcGFnZSB3aGlsZSBpdCBsb2Fkc1xyXG4gICAgICAgIGlmICh3YWl0KSB0aGlzLnNldFN0YXRlKHt3YWl0aW5nOnRydWV9KTtcclxuXHJcbiAgICAgICAgLy9UaGUgQVBJIHJlcXVlc3RcclxuICAgICAgICByZXR1cm4gZmV0Y2ggKHdpbmRvdy51cmwodGFyZ2V0KSwgb3B0aW9ucykuXHJcbiAgICAgICAgdGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXJlcy5vayAmJiByZXMuc3RhdHVzID09IDUwMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZXJyb3I6cmVzLnN0YXR1c1RleHR9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICghcmVzLm9rICYmIHJlcy5zdGF0dXMgPT0gNDAxKSB7XHJcbiAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKFwidG9rZW5cIik7XHJcblxyXG4gICAgICAgICAgICAgICAgLy9TZXQgc3RhdGVcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRva2VuOiAgbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICB1c2VyOiAgIG51bGwsXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvL1JlZW5hYmxlIHRoZSBwYWdlIGFmdGVyIGl0IGxvYWRzXHJcbiAgICAgICAgICAgIGlmICh3YWl0KSB0aGlzLnNldFN0YXRlKHt3YWl0aW5nOmZhbHNlfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gcmVzO1xyXG4gICAgICAgIH0pLlxyXG4gICAgICAgIHRoZW4ocmVzID0+IHJlcy5qc29uKCkpLlxyXG4gICAgICAgIHRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAvL1JldHVybmVkIGEgbm9uIGZhdGFsIGVycm9yIGZyb20gdGhlIHNlcnZlclxyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UuZXJyb3IgJiYgdG9hc3RpZnkpIHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuZXJyb3IpICYmICEodHlwZW9mIHJlc3BvbnNlLmVycm9yID09PSAnb2JqZWN0JykpIHtcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdC53YXJuKHJlc3BvbnNlLmVycm9yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpbmRleCBpbiByZXNwb25zZS5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdC53YXJuKHJlc3BvbnNlLmVycm9yW2luZGV4XVswXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChvbkVycm9yKSBvbkVycm9yKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy9FdmVyeXRoaW5nIHdlbnQgcmlnaHRcclxuICAgICAgICAgICAgZWxzZSBpZiAocmVzcG9uc2Uuc3VjY2VzcyAmJiB0b2FzdGlmeSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlLnN1Y2Nlc3MpICYmICEodHlwZW9mIHJlc3BvbnNlLnN1Y2Nlc3MgPT09ICdvYmplY3QnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0LnN1Y2Nlc3MocmVzcG9uc2Uuc3VjY2Vzcyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpbmRleCBpbiByZXNwb25zZS5zdWNjZXNzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0LnN1Y2Nlc3MocmVzcG9uc2Uuc3VjY2Vzc1tpbmRleF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAob25TdWNjZXNzKSBvblN1Y2Nlc3MocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvL05vIGZlZWRiYWNrIGdpdmVuLCBqdXN0IGNvbnNpZGVyIHN1Y2Nlc3NmdWxsXHJcbiAgICAgICAgICAgIGVsc2UgaWYgKG9uU3VjY2Vzcykge1xyXG4gICAgICAgICAgICAgICAgb25TdWNjZXNzKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vRWxhYm9yYXRlZCBmZXRjaCB0aGF0IGhhbmRsZXMgYXV0aFxyXG4gICAgYXBpICh0YXJnZXQsIG9wdGlvbnMsIG9uU3VjY2VzcyA9IG51bGwsIG9uRXJyb3IgPSBudWxsKSB7XHJcbiAgICAgICAgbGV0IHByZWhlYWRlciA9IHtcclxuICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6ICBcIkJlYXJlciBcIiArIHRoaXMuc3RhdGUudG9rZW4sXHJcbiAgICAgICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiAgIFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICAgICAgXCJBY2NlcHRcIjogICAgICAgICBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB3YWl0OnRydWUsXHJcbiAgICAgICAgICAgIHRvYXN0aWZ5OnRydWVcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5mZXRjaChcImFwaS9cIiArIHRhcmdldCwgT2JqZWN0LmFzc2lnbihwcmVoZWFkZXIsIG9wdGlvbnMpLCBvblN1Y2Nlc3MsIG9uRXJyb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gUmVkaXJlY3QgZnVuY3Rpb25zXHJcbiAgICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICByZWRpcmVjdCAodXJsID0gXCIvXCIpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcclxuICAgICAgICAgICAgcmVkaXJlY3R1cmw6ICAgIHVybCxcclxuICAgICAgICAgICAgcmVkaXJlY3RlZDogICAgIGZhbHNlLFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gRXJyb3IgZnVuY3Rpb25zXHJcbiAgICAvLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvL1dpbGwgZGlzcGxheSBhbiBlcnJvciBhbmQgcHJldmVudCB0aGUgcmVhY3QgZnJvbSBjcmFzaGluZ1xyXG4gICAgZXJyb3IgKGVycm9yKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZXJyb3J9KTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wb25lbnREaWRDYXRjaChlcnJvciwgaW5mbykge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2Vycm9yfSk7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcblxyXG4vL1RoaXMgd2lsbCBiZSByZXNwb25zaWJsZSBmb3IgaGFuZGxpbmcgZ2xvYmFsIHN5c3RlbSBjYWxscyBhbmQgZm93YXJkIGl0IHRvIHRoZSBjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBTeXN0ZW0gKHtvYmplY3QsIGNoaWxkcmVufSkgOiBhbnkge1xyXG5cclxuICAgIC8vV2UgYXJlIGdvaW5nIHRvIGJpbmQgYSBzeXN0ZW0vYXBwbGljYXRpb24gc3RhdGUgdG8gZXZlcnl0aGluZ1xyXG4gICAgdmFyIFsgc3lzdGVtLCBzZXRTeXN0ZW0gXSA9IFJlYWN0LnVzZVN0YXRlKHtcclxuICAgICAgICBmZXRjaDogICAgb2JqZWN0LmZldGNoLFxyXG4gICAgICAgIGFwaTogICAgICBvYmplY3QuYXBpLFxyXG4gICAgICAgIHJlZGlyZWN0OiBvYmplY3QucmVkaXJlY3QsXHJcbiAgICAgICAgZXJyb3I6ICAgIG9iamVjdC5lcnJvcixcclxuICAgICAgICB1c2VyOiAgICAgb2JqZWN0LnN0YXRlLnVzZXJcclxuICAgIH0pO1xyXG5cclxuICAgIC8vQmluZCB0byB3aW5kb3cgb2JqZWN0XHJcbiAgICB3aW5kb3cuc3lzdGVtICAgICA9IHN5c3RlbTtcclxuICAgIHdpbmRvdy5zZXRTeXN0ZW0gID0gc2V0U3lzdGVtO1xyXG5cclxuICAgIC8vRm9yY2UgYSBudWxsIHJlc3BvbnNlXHJcbiAgICByZXR1cm4gY2hpbGRyZW47XHJcbn0iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=