<!DOCTYPE html>
<html>
<head>
	<title>Boilerplate</title>

    <!-- Metas -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="og:description" content="{{ env('APP_DESCRIPTION') }}">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body>
	<!-- Application -->
	<div id="app"></div>

	<!-- Scripts -->
	<section>
        <script src="js/manifest.js"></script>
        <script src="js/vendor.js"></script>
        <script src="js/index.js"></script>
	</section>
</body>
</html>